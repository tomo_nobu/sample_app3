Rails.application.routes.draw do
  get 'password_reset/new'

  get 'password_reset/edit'

  # get 'users/new'
  root 'static_pages#home'
  # get 'static_pages/home'
  get '/help', to: 'static_pages#help'
  get '/about', to: 'static_pages#about'
  get '/contact', to: 'static_pages#contact'
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  get '/login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only:[:edit]
  # GET "/account_activations/<token>/edit
  #  params[:token] <=== 有効化トークン(activation_token)
  #  Controller: params[:id]
  resources :password_resets, only:[:new, :create, :edit, :update]
  resources :microposts, only:[:create, :destroy]
  resources :relationships, only: [:create, :destroy]
end
